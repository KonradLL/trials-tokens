let reactionInterval;

const ON = "on", OFF = "off";
"VideoOverlay_container__"

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
	debugger;
	if (message.type === ON) {
		ActivateInterval();
	} else if (message.type === OFF) {
		DeactivateInterval();
	} else {
		console.log(message);
	}
});

const overlays = document.querySelectorAll('div[class*="VideoOverlay_container__"]');
const observer = new MutationObserver((mutations) => {
	console.log(mutations);
	debugger;

	mutations.forEach(mutation => {
		let reactionIsActive = mutation.attributeName === 'class';
		if (reactionIsActive) {
			console.log(mutation.target);
		}
	});
	
});



function DeactivateInterval() {
	observer.disconnect();
}

function ActivateInterval() {
	reactionInterval = setInterval(selectActiveReaction, 2 * 1000);
	observer.observe(overlays, {
		childList: true,
		attributes: true
	});
}

function selectActiveReaction() {
	let activeReaction = document.querySelectorAll('div[class*="VideoOverlay_reactionVisible__"]');
	if (activeReaction.length <= 0) return false;

	debugger;
	const reactions = document.querySelectorAll('div[class^="Reaction_actions__"]')[0].children;
	let randomReaction = reactions[Math.floor(Math.random() * reactions.length)];
	randomReaction.click();
	return true;
}




