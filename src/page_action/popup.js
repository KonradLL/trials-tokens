const ID = "farm";
document.addEventListener("DOMContentLoaded", init, false);

function init() {
	let input = document.querySelector("input");
	input.addEventListener("change", onChange, false);
	chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		chrome.runtime.sendMessage({ type: "getState", id: tabs[0].id}, (res) => {
			input.checked = res;
		});
	});
	

	function onChange() {
		if (this.checked) {
			chrome.runtime.sendMessage({ type: "farm", state: "on" });
		} else {
			chrome.runtime.sendMessage({ type: "farm", state: "off" });
		}
	}
}
