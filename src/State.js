
class State {
	static states = {};

	static save(id, state) {
		const isOn = (state === "on") ? true : false;
		this.states[id] = isOn;
	}
	
	static get(id) {
		return this.states[id];
	}
	
	static remove(id) {
		delete this.states[id];
	}
}