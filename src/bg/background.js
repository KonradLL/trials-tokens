const POPUP_PATH = "/src/page_action/page_action.html";
const ON = "on", OFF = "off"
const ACTIVE_IMG = "../../icons/trials-active-icon.png";
const INACTIVE_IMG = "../../icons/trials-icon.png";

//enable extension for twitch tabs
chrome.tabs.query({ url: "https://www.twitch.tv/*" }, function (tabs) {
	console.log(tabs);
	tabs.forEach((tab) => {
		chrome.pageAction.show(tab.id);
	});
});

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
	const isTwitch = tab.url && tab.url.includes("twitch.tv");
	if (isTwitch & (changeInfo.status === "loading")) {
		chrome.pageAction.show(tabId);
	}
});

chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
	State.remove(tabId);
});

//example of using a message handler from the inject scripts
chrome.runtime.onMessage.addListener(async function (message, sender, sendResponse) {
	if (message.type === "getState") {
		let state = State.get(message.id);
		sendResponse(state ? state : false);
		// return true; // -- keep connection alive for callback
	}

	if (message.type === "farm") {
		chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
			setFarm(tabs[0].id, message.state);
		});
	}
});

function setFarm(tabId, state) {
	State.save(tabId, state);
	chrome.tabs.sendMessage(tabId, { type: state });

	let details = {tabId: tabId};
	details.path = (state === "on") ? ACTIVE_IMG : INACTIVE_IMG;
	chrome.pageAction.setIcon(details);
}